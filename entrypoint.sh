#!/bin/bash

#should map UIDs/GIDs in the docker container from a list
#tested on debian using debian container

HOST_PASSWD="/tmp/passwd"
U_TO_COPY="/tmp/user_sync.txt"
PROCESSED_U="/tmp/usersync.log"

#sync uids
echo "syncing users" 
while read USER; do
        U_UID=$(grep $USER $HOST_PASSWD  | awk -F ":" {'print $3'})
        
        #check in /etc/login.defs to confirm minium UID is 1000
        if (( $U_UID > 999 )) ; then

                U_NAME=$(grep $USER $HOST_PASSWD  | awk -F ":" {'print $1'})
                G_UID=$(grep $USER $HOST_PASSWD  | awk -F ":" {'print $4'})
                echo "$U_NAME" >> $PROCESSED_U
                groupadd -g $G_UID $U_NAME
                useradd -u $U_UID -g $G_UID $U_NAME
        fi
done < $HOST_PASSWD